import matplotlib.pyplot as plt
import numpy as np
import rosbag_pandas


def import_tof_data(filename, plots=False):
    """ Imports tof experimental data from rosbag file in filename
    plots is bool, if True plots are generted, default False
    Returns df a dataframe with raw values (including outliers)
    """    
    print("importing rosbag file %s ..." % filename )
    df = rosbag_pandas.bag_to_dataframe(filename)    
#    C = list(df) # list of columns in rosbag file
    Ctof = (s for s in df if s[0:3]  == "tof") # extract columns with tof data
    if plots:   
        print("plotting RAW TOF data...")   
        plt.figure(1)
        df[Ctof].plot(style = '.-')
        plt.grid()
        plt.title("Original RAW TOF data")
        plt.legend()
    return df


def clean_tof_data(df, threshold=0.3, tof_id = ["tof","anchor"], plots=False):
    """ Otlier rejection of tof experimental data from rdataframe previously imported with import_tof_data
    threshold is used for outlier rejection using a median filter, default value 0.3
    plots is bool, if True plots are generted, default False
    Returns dff a dataframe with filtered values (without outliers)
    """    
    #C = list(df) # list of columns in rosbag file
    tof_id = ["tof","anchor"]
    Ctof = [c for s in tof_id for c in df if c.startswith(s)] # extract columns with tof data, that start with either "tof" or "anchor"
    print("Removing large outliers: remove values larger than 100m...")       
    df[df.loc[:,Ctof].abs()>100] = np.nan # removes all values greater than 100, outliers
    df[df.loc[:,Ctof]<0] = np.nan # removes negative values 
    df[df.loc[:,Ctof].abs()<0.2] = np.nan # removes ranges smaller than 20cm, not reliable
    if plots:   
        print("plotting RAW TOF data...")   
        plt.figure(2)
        df[Ctof].plot(style='.-')
        plt.grid()
        plt.title("Original RAW TOF data without large outliers")
        plt.legend()
    #threshold = 0.3 # max allowed difference in meters between raw and median filtered
    print("Removing outliers using median filter and threshold=%s ..." % threshold)      
    dff = df.copy()  # creates copy, filtered data
    if plots:
        kw = dict(marker='o', linestyle='none', color='r', alpha=0.3)
        fig, ax = plt.subplots() 
    for c in Ctof:
        #c = Ctof[0]
        print("Removing outliers from %s" % c)      
        cmf = "mf_"+c # new column with median filtered data mf_tof...
        df[cmf]= df[c].dropna().rolling(window=5,center=True).median().fillna(method='bfill').fillna(method='ffill')
        difference = np.abs(df[c]- df[cmf])
        outlier_idx = difference > threshold # contains index of outliers
        if plots:
            print("plotting raw TOF data and detected outliers...")   
            df[c].plot(style = '.-', ax = ax, label = c)
            #dfmf.plot(style = '.', ax = ax)
            df.ix[outlier_idx,c].plot( ax = ax,label = None, **kw)
        dff.ix[outlier_idx,c] = np.nan # remove outliers, replace with nan
    if plots:
        plt.grid()
        plt.title("Raw TOF data and detected outliers")
        plt.legend()
        
        print("plotting fltered TOF data...")   
        plt.figure(4)
        dff[Ctof].plot(style = '.-')
        plt.title("Filtered TOF data")
        plt.legend()
        plt.grid()

    return dff




