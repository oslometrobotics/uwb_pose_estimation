import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import rosbag_pandas
import os
import sys
import pandas as pd
import rosbag_import



currentdir = os.path.dirname(__file__)

#filename = os.path.join(currentdir,os.pardir,"data/experiment1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment1_2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment4.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment5.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_4.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_5.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_6.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment7.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment8.bag")

#filename = os.path.join(currentdir,os.pardir,"data/experiment6_1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment8.bag")
filename = os.path.join(currentdir,os.pardir,"data/experiment8.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment6_3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment6_3.bag")





df = rosbag_import.import_tof_data(filename, plots = False)

#%%
dff = rosbag_import.clean_tof_data(df,threshold=0.3, plots = False)

#%%
dffrs = dff.resample("100L",loffset="50L").mean()


# pkl files were created using import_data.py
# loading of pkl files is much faster than importing the rosbag files
#pkl_filename = os.path.join(currentdir,os.pardir,"data/df_experiment3.pkl")
#print("loading pickle file %s " % pkl_filename)
#df = pd.read_pickle(pkl_filename)
#
#pkl_filename = os.path.join(currentdir,os.pardir,"data/dff_experiment3.pkl")
#print("loading pickle file %s " % pkl_filename)
#dff = pd.read_pickle(pkl_filename)

##%%
#print("Resampling and interpolating dataframe...")
## resample data every 100ms
#rs_freq = "100L" # 100 milliseconds
#dffrs = dff.resample(rs_freq, how='mean')
## interpolate missing data
#dffint = dffrs.interpolate(method='spline', order=3)

#%%

C = list(df) # list of columns in rosbag file
tof_id = ["tof","anchor"]
Ctof = [c for s in tof_id for c in df if c.startswith(s)] # extract columns with tof data, that start with either "tof" or "anchor"
  

       
       
       
#%%
fig, ax = plt.subplots()        
dff.ix[:,Ctof].plot(style=".",ax=ax)
dffrs.ix[:,Ctof].plot(style="x",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()
#%%    

#%%
fig, ax = plt.subplots()        
dffrs.ix[:,Ctof].plot(style=".",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()

#%%
id = 10
fig, ax = plt.subplots()        
dff.ix[:,Ctof[id]].plot(style=".",ax=ax)
dffrs.ix[:,Ctof[id]].plot(style="x",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()
plt.show()
#%%    

t0 = "2017-02-27 15:07:00"
tf = "2017-02-27 15:07:50"
fig, ax = plt.subplots()        
dffrs.ix[t0:tf,Ctof].plot(style=".-",ax=ax)
#dffrs.ix[t0:tf,Ctof].plot(style="-",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
ax.legend_.remove()
plt.ylabel("Ranges [m]")
plt.xlabel("Time")


#%%
Cpos = [c for c in df if c.startswith("qualisys_tagArray_pose__pose_position")] 
fig, ax = plt.subplots()        
#dff.ix[t0:tf,Cpos].plot(style=".",ax=ax)
#dffrs.ix[t0:tf,Cpos].plot(style=".",ax=ax)
dffrs.ix[:,Cpos].plot(style=".",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()

    
    
#%%


# Anchor positions measured manually
# experiments 1-3
A1 = np.array([[0.99,0.49,0,-0.01],[-0.02,0.46,0.99,0.04],[0.04,0.29,0.04,0.04]])
# experiments 4-5
A2 = np.array([[1.89,1.98,0.1,-0.01],[-0.02,0.89,2.0,0.04],[0.04,0.29,0.04,0.04]])


# Resample and interpolate data

# Figures 1
# Compute tag position estimate using tof data
# plot anchors position given by qualisys
# plot tag position given by qualisys
# 3D scater
# (3,1) subplot
  
# Figure 2
# plot tag pose given by qualisys
# plot anchor pose given by qualisys
# plot tag position given by trilateration
# 3D scater
# (6,1) subplot position and Euler angles

# Figure 3
# plot tag position estimation errors qualisys - uwb
# (6,1) subplot position and Euler angles

# Figure 4
# TOF Residuals trilateration - characterize tof errors
