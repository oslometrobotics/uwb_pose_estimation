import matplotlib.pyplot as plt
import numpy as np
from uwbpose import *
from trilateration_mlr import *




def test_hessian():
    N = 200 # number fo points
    F = np.zeros(N) # array to store mlr cost function values
    t = .1
    T = np.linspace(-5*t,10*t,N) # step size index        
    d = np.random.randn(3) # random direction
    p = 20*np.random.randn(3) # random initial estimate
    pest = p + 5*np.random.randn(3) # random initial estimate
    A = np.random.randn(3,4)
    sigma = 0.1
    mranges = get_measured_ranges1(A,p,sigma)
    f0 = mlr1_cost(A,p,mranges)
    g = gradient_mlr1(A,p,mranges)
    H = hessian_mlr1(A,p,mranges)
    #d = -g
    for i in range(N):
        pi = p + T[i]*d
        F[i] = mlr1_cost(A,pi,mranges)
    F1 = f0 + T*g.dot(d) # first order aproximation of mlr cost function
    F2 = f0  + T*g.dot(d) + 0.5*T*T*d.dot(H).dot(d) # first order aproximation of mlr cost function
    plt.figure(1)
    plt.plot(T,F, "b-", label = "Cost function")
    plt.plot(T,F1, "k-.", label = "First order approximation")
    plt.plot(T,F2, "g-.", label = "Second order approximation")
    plt.plot(0,f0,"bo", label = "Initial")
    plt.grid()
    plt.xlabel("step size t")
    plt.ylabel("ML-R cost function")
    plt.legend()

#test_hessian()
   
#%%
def test_armijo():
    N = 200 # number fo points
    F = np.zeros(N) # array to store mlr cost function values
    p = 5*np.random.randn(3) # random initial estimate
    pest = p + 5*np.random.randn(3) # random initial estimate
    A = np.random.randn(3,4)
    sigma = 0.001
    mranges = get_measured_ranges1(A,p,sigma)
    f0 = mlr1_cost(A,p,mranges)
    g = gradient_mlr1(A,p,mranges)
    H = hessian_mlr1(A,p,mranges)
    #d = np.random.randn(3) # random direction
    d = -g # negative gradient descent
    (pt,m,t) = armijo(A,p,mranges,d)
    ft = mlr1_cost(A,pt,mranges)
    T = np.linspace(-5*t,10*t,N) # step size index        
    for i in range(N):
        pi = p + T[i]*d
        F[i] = mlr1_cost(A,pi,mranges)
    F1 = f0 + T*g.dot(d) # first order aproximation of mlr cost function
    F2 = f0  + T*g.dot(d) + 0.5*T*T*d.dot(H).dot(d) # first order aproximation of mlr cost function
    plt.figure(1)
    plt.plot(T,F, "b-", label = "Cost function")
    plt.plot(T,F1, "k-.", label = "First order approximation")
    plt.plot(T,F2, "g-.", label = "Second order approximation")
    plt.plot(t,ft,"ko", label = "Armijo step size")
    plt.plot(0,f0,"bo", label = "Initial")
    plt.grid()
    plt.xlabel("step size t")
    plt.ylabel("ML-R cost function")
    plt.legend()

test_armijo()



p = 10*np.random.randn(3) # random oisition
#pest = p + 20*np.random.randn(3) # random initial estimate
A = np.random.randn(3,4)
sigma = 0.05
mranges = get_measured_ranges1(A,p,sigma)

pest = trilaterateration_mlr1(A,mranges,p0=None,Nmax=100, eps = 1e-8)
pestls = trilateration_ls(A,mranges)


print(p)
print(pest)
print(pestls)
