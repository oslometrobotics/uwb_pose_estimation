import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import rosbag_pandas
import os
import sys
import pandas as pd
import rosbag_import
import uwbpose
import transformations 



currentdir = os.path.dirname(__file__)

#filename = os.path.join(currentdir,os.pardir,"data/experiment1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment1_2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment4.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment5.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_4.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_5.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment6_6.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment7.bag")
#filename = os.path.join(currentdir,os.pardir,"data/Experiment8.bag")

#filename = os.path.join(currentdir,os.pardir,"data/experiment6_1.bag")
filename = os.path.join(currentdir,os.pardir,"data/experiment7.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment7.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment6_3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment6_3.bag")





df = rosbag_import.import_tof_data(filename, plots = False)

#%%
dff = rosbag_import.clean_tof_data(df,threshold=0.3, plots = False)

#%%
dffrs = dff.resample("100L",loffset="50L").mean()


# pkl files were created using import_data.py
# loading of pkl files is much faster than importing the rosbag files
#pkl_filename = os.path.join(currentdir,os.pardir,"data/df_experiment3.pkl")
#print("loading pickle file %s " % pkl_filename)
#df = pd.read_pickle(pkl_filename)
#
#pkl_filename = os.path.join(currentdir,os.pardir,"data/dff_experiment3.pkl")
#print("loading pickle file %s " % pkl_filename)
#dff = pd.read_pickle(pkl_filename)

##%%
#print("Resampling and interpolating dataframe...")
## resample data every 100ms
#rs_freq = "100L" # 100 milliseconds
#dffrs = dff.resample(rs_freq, how='mean')
## interpolate missing data
#dffint = dffrs.interpolate(method='spline', order=3)

#%%

C = list(df) # list of columns in rosbag file
tof_id = ["tof","anchor"]
Ctof = [c for s in tof_id for c in df if c.startswith(s)] # extract columns with tof data, that start with either "tof" or "anchor"
  

       
       
       
#%%
fig, ax = plt.subplots()        
dff.ix[:,Ctof].plot(style=".",ax=ax)
dffrs.ix[:,Ctof].plot(style="x",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()
#%%    

#%%
fig, ax = plt.subplots()        
dffrs.ix[:,Ctof].plot(style=".",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()

#%%
id = 10
fig, ax = plt.subplots()        
dff.ix[:,Ctof[id]].plot(style=".",ax=ax)
dffrs.ix[:,Ctof[id]].plot(style="x",ax=ax)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()
plt.show()
#%%    

Cpos = [c for c in df if c.startswith("qualisys_tagArray_pose__pose_position")] 
Corient = [c for c in df if c.startswith("qualisys_tagArray_pose__pose_orientation")] 

fig, ax1 = plt.subplots()        
dffrs.ix[:,Ctof].plot(style=".",ax=ax1)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
#plt.legend()
ax.legend_.remove()

#fig, ax = plt.subplots()
plt.figure(2)
ax2 = plt.subplot(211, sharex=ax1)
#dff.ix[t0:tf,Cpos].plot(style=".",ax=ax)
#dffrs.ix[t0:tf,Cpos].plot(style=".",ax=ax)
dffrs.ix[:,Cpos].plot(style=".",ax=ax2)
plt.ylabel("Position")
plt.grid()
ax3 = plt.subplot(212, sharex=ax1)
dffrs.ix[:,Corient].plot(style=".",ax=ax3)
plt.ylabel("Orientation")
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend()
plt.show()

#%%
t0 = "2017-02-27 15:02:30"
tf = "2017-02-27 15:03:30"
dft =dffrs[t0:tf]
#dft = dft.resample("100L",loffset="50L").mean()


fig, ax1 = plt.subplots()        
dft.ix[:,Ctof].plot(style=".",ax=ax1)
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
#plt.legend()
ax1.legend_.remove()

#fig, ax = plt.subplots()
plt.figure(2)
ax2 = plt.subplot(211, sharex=ax1)
#dff.ix[t0:tf,Cpos].plot(style=".",ax=ax)
#dffrs.ix[t0:tf,Cpos].plot(style=".",ax=ax)
dft.ix[:,Cpos].plot(style=".",ax=ax2)
plt.ylabel("Position")
plt.grid()
plt.legend(["x","y","z"])
ax3 = plt.subplot(212, sharex=ax1)
dft.ix[:,Corient].plot(style=".",ax=ax3)
plt.ylabel("Orientation")
#dffint.ix[:,Ctof].plot(style="s",ax=ax)
plt.grid()
plt.legend(["qw","qx","qy","qz",])
plt.show()



    
#%%

# select a small portion of df
dft = dft.ix[0:20,:]

## Anchor positions measured manually
## experiments 1-3
#A1 = np.array([[0.99,0.49,0,-0.01],[-0.02,0.46,0.99,0.04],[0.04,0.29,0.04,0.04]])
## experiments 4-5
#A2 = np.array([[1.89,1.98,0.1,-0.01],[-0.02,0.89,2.0,0.04],[0.04,0.29,0.04,0.04]])

# experiments 6-8 measured manually
A = np.array([[0.75, 0.0,  -0.85, 0.0,  49.5 ],
              [ 0.0,  -0.75, 0.0, 0.75, 36.5],
              [ 0.0,  0.0,  0.0,  0.0, 100.0]])

B = np.array([[0.643, 0.503, 0.213, 0.0,   0.245 ],
              [0.228, 0.0,   0.0,   0.232, 0.445],
              [0.046, 0.146, 0.046, 0.046, 0.196]])
offset = np.array([0.3312, 0.21793, 0.08946])
B = B - offset[:,np.newaxis]

N = len(dft.index) # number of rows in dft
nA = A.shape[1]
nB = B.shape[1]

S = uwbpose.simrun(N)
S.A = A
S.B = B
S.sigma = 0.03 # range error std

PT = np.zeros((N,3)) # contains position of B fram
RT = np.zeros((N,3,3)) # contains rotation matrix of B frame
mranges = np.zeros((N,nA,nB)) # contains measured ranges
PT = dft.ix[:,Cpos].as_matrix()
for i in range(N):
    q = dft.ix[i,Corient].as_matrix()
    R = transformations.quaternion_matrix(q)[0:3,0:3]
    RT[i,:] = R
    #mranges[i,:] = dft.ix[i,Ctof].as_matrix().reshape(nA,nB).T 
    mranges[i,:] = dft.ix[i,Ctof].as_matrix().reshape(nA,nB)

S.PT = PT # sets trajectory position
S.RT = RT # sets trajectory rotation
S.mranges = mranges

#%%

#%%
alg1 = uwbpose.repeater
alg2 = uwbpose.gradient_descent # sets algorithm to use

#S.run(alg1, "ls") # run algorithm
S.run(alg1, "mlr") # run algorithm
#S2.run(alg1, "ls") # run algorithm
#S.run(alg2) # run algorithm

#%%
uwbpose.plot_run(S) # plot results
#plot_run(S2)






 

#