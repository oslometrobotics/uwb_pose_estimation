import numpy as np
#from scipy.linalg import expm
#from math import pi
import uwbpose
import pytest


def test_skew():    
    w1 = 1
    w2 = 2
    w3 = 3
    w = np.array([w1,w2,w3]) 
    S = np.array([[0,-w3,w2],[w3,0,-w1],[-w2,w1,0]]) 
    assert uwbpose.skew(w)==S

