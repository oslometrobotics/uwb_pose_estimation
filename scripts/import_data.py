import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import rosbag_pandas
import os
import sys
import pandas as pd
import rosbag_import



#filename = os.path.join(currentdir,os.pardir,"data/experiment1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment1_2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment4.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment5.bag")


#df = rosbag_import.import_tof_data(filename, plots = True)
#dff = rosbag_import.clean_tof_data(df,threshold=0.3, plots = True)


currentdir = os.path.dirname(__file__)


#exp_str = ["experiment1","experiment1_2","experiment2","experiment3","experiment4","experiment5"]
exp_str = ["experiment1_2","experiment2","experiment3","experiment4","experiment5"]


for str in exp_str:
    bagfilename = os.path.join(currentdir,os.pardir,"data/"+str+".bag")
    df = rosbag_import.import_tof_data(bagfilename, plots = False)
    dff = rosbag_import.clean_tof_data(df,threshold=0.3, plots = False)

    df_pklfilename = os.path.join(currentdir,os.pardir,"data/df_"+str+".pkl")
    dff_pklfilename = os.path.join(currentdir,os.pardir,"data/dff_"+str+".pkl")
    # Save dataframes to pickle binary files that can be loaded later more efficiently
    print("writing raw dataframe df to %s" % df_pklfilename)
    df.to_pickle(df_pklfilename)
    print("writing filtered dataframe dff to %s" % dff_pklfilename)
    dff.to_pickle(dff_pklfilename)
    
    
    





