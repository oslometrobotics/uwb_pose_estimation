import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import rosbag_pandas
import os
import sys
import pandas as pd


currentdir = os.path.dirname(__file__)

filename = os.path.join(currentdir,os.pardir,"data/experiment2.bag")




#%%

print("importing rosbag file %s ..." % filename )
df = rosbag_pandas.bag_to_dataframe(filename)


#%%
C = list(df) # list of columns in rosbag file

Ctof = [s for s in C if s[0:3]  == "tof"] # extract columns outlier_idxith tof data

print("Removing outliers...")
df[df.loc[:,Ctof].abs()>100] = np.nan # removes all values greater than 100, outliers


print("plotting...")   
df.plot(y = Ctof, style = '.-')
plt.grid()

#%%

c = Ctof[0]
cmf = c+"mf" # new column with median filtered data
df[cmf]= df[c].dropna().rolling(window=3,center=True).median().fillna(method='bfill').fillna(method='ffill')
# fillna(method='bfill').fillna(method='ffill')
fig, ax = plt.subplots()
df[c].plot(style = 'b.-', ax = ax)
df[cmf].plot(style = 'g.', ax = ax)

#%%

difference = np.abs(df[c]- df[cmf])
#difference.plot(marker=".")
threshold = 1
outlier_idx = difference > threshold # contains index of outliers

#%%
kw = dict(marker='o', linestyle='none', color='r', alpha=0.3)
fig, ax = plt.subplots()
df[c].plot(style = '.-', ax = ax)
#dfmf.plot(style = '.', ax = ax)
df[c][outlier_idx].plot( ax = ax,**kw)
plt.grid()

#%%%

