
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.linalg import expm
import pdb
from math import pi
import os
import sys
sys.path.append(os.getcwd())
import transformations as tf

def skew(w):
    w1 = w[0]
    w2 = w[1]
    w3 = w[2]
    return np.array([[0,-w3,w2],[w3,0,-w1],[-w2,w1,0]])   

def plot_frame(p,R):
    # plots 3 vectors reference frame centered in p and orientation R
    # p is a 3x1 array - origin of frame
    # R is a 3x3 array - rotation matrix
    #plt.quiver(p[0],p[1],p[2],R[0,0],R[1,0],R[2,0],color="k",linewidth=2) # X 
    plt.quiver(p[0]+R[0,0],p[1]+R[1,0],p[2]+R[2,0],R[0,0],R[1,0],R[2,0],color="k",linewidth=2) # X 
    plt.quiver(p[0]+R[0,1],p[1]+R[1,1],p[2]+R[2,1],R[0,1],R[1,1],R[2,1],color="b",linewidth=2) # X 
    plt.quiver(p[0]+R[0,2],p[1]+R[1,2],p[2]+R[2,2],R[0,2],R[1,2],R[2,2],color="g",linewidth=2) # X 

def plot_array(ax,A,p,R):
    # plots beacon array in matrix A in position p and orientation given by R
    # A is nxnA
    # p is 3x1
    # R is 3x3
    #X = R@A + p # X contains beacon coordinates in "inertial" frame
    #pdb.set_trace()
    #print(p)
    #print(np.dot(R,A))
    X = np.dot(R,A) + p # X contains beacon coordinates in "inertial" frame
    nA = A.shape[1] # number of columns
    ax.scatter(X[0,:],X[1,:],X[2,:],zdir='z', s=100) # plots beacon positions
    for i in range(nA):
        plt.plot([p[0,0], X[0,i]],[p[1,0],  X[1,i]],[p[2,0], X[2,i]],linewidth=2)


class simsetup:
    """ Simulation setup"""    
    nA = 4 # number of beacons in vehicle 1
    nB = 4 # number of beacons in vehicle 2
    #A = np.random.randn(3,nA) # random coordinates of beacons
    #B = np.random.randn(3,nB) # random coordinates of beacons
    A = np.c_[np.zeros((3,1)),1*np.eye(3),np.random.randn(3,nA-4)]
    B = np.c_[np.zeros((3,1)),1*np.eye(3),np.random.randn(3,nB-4)]
    pA = np.zeros((3,1)) # origin
    RA = np.eye(3) # identity matrix
    pB = 100*np.random.randn(3,1) # origin
    w = 1*np.random.randn(3,1) # origin
    RB = expm(skew(w)) # identity matrix
    #pB = pA
    #RB = RA
    sigma = 0.1 # range error standard deviation

    def get_D(self):
        # returns matrix of square range measurements
        R = self.RB
        p = self.pB
        B = self.B
        A = self.A
        nA = self.nA
        nB = self.nB
        D = np.zeros((nA,nB)) # initialize
        #pdb.set_trace()
        for i in range(nA):
            for j in range(nB):
                z = R.dot(B[:,j])+p.flat -A[:,i] # distance vector
                # print(z)
                D[i,j]= np.dot(z.T,z) 
        return D
    
    def get_Dm(self):
        # compute measurements with noise
        nA = self.nA
        nB = self.nB
        sigma = self.sigma
        D = self.get_D() # noise free squared ranges
        r = np.sqrt(D) # square root, ideal range  matrix
        rm = r + sigma*np.random.randn(nA,nB) #range measurements matrix         
        Dm = rm*rm
        return Dm
        
    def plot_simsetup(self):
        fig = plt.figure(10)
        plt.hold(True)
        ax = fig.add_subplot(111, projection='3d')
        plot_frame(self.pA,self.RA)
        plot_frame(self.pB,self.RB)
        plot_array(ax,self.A,self.pA,self.RA)
        plot_array(ax,self.B,self.pB,self.RB)
        plt.axis('equal')
        plt.show()
        
    def repeater(self,Dm):
        # RangE only PosE estimAtion using TrilatERation-procustes 
        # Dm is matrix of squared ranged measurements nAxnB
        # returns estimated position and rotation matrix (Rest,pest)
        B = self.B
        A = self.A
        nA = self.nA
        nB = self.nB
        Ma = np.eye(nA)-1/nA*np.ones((nA,nA))
        Mb = np.eye(nB)-1/nB*np.ones((nB,nB))
        Ac = A.dot(Ma)
        T = 1/2*np.linalg.inv(Ac.dot(Ac.T)).dot(Ac)
        Ba = T.dot(np.tile(np.diagonal(A.T.dot(A)),(nB,1)).T-Dm)
        G = Ba.dot(Mb).dot(B.T)
        U,s,V = np.linalg.svd(G,full_matrices=1)
        q = np.linalg.det(U.dot(V))
        Rest = U.dot(np.diag([1,1,q])).dot(V)
        pest = 1/nB*(Ba-Rest.dot(B)).dot(np.ones((nB,1)))
        return (Rest,pest)

def test_get_D():
    S = simsetup() 
    S.plot_simsetup()
    S.get_D()
    R = S.RA
    p = S.pA
    B = S.B
    A = S.A
    nA = S.nA
    nB = S.nB
    D = np.zeros((nA,nB)) # initialize
    j=1
    i=1
    z = np.dot(R,B[:,j])+p[:,0]-A[:,i]
    for i in range(nA):
        for j in range(nB):
            z = np.dot(R,B[:,j])+p[:,0]-A[:,i] # distance vector
            #print(z)
            D[i,j]= np.dot(z.T,z) 


def test_repeater():
    # debug
    S = simsetup(); 
    D = S.get_Dm()
    R = S.RB
    p = S.pB
    B = S.B
    A = S.A
    nA = S.nA
    nB = S.nB
    Ma = np.eye(nA)-1/nA*np.ones((nA,nA))
    Mb = np.eye(nB)-1/nB*np.ones((nB,nB))
    Ac = A.dot(Ma)
    T = 1/2*np.linalg.inv(Ac.dot(Ac.T)).dot(Ac)
    Ba = T.dot(np.tile(np.diagonal(A.T.dot(A)),(nB,1)).T-D)
    G = Ba.dot(Mb).dot(B.T)
    U,s,V = np.linalg.svd(G,full_matrices=1)
    q = np.linalg.det(U.dot(V.T))
    Rest = U.dot(np.diag([1,1,q])).dot(V)
    pest = 1/nB*(Ba-Rest.dot(B)).dot(np.ones((nB,1)))
    print(R)
    print(Rest)
    print(p)
    print(pest)    
    

def spiral_trajectory(N,zmax,rmax,Nturns):
    # computes spiral trajectory
   #t = range(N)
    t = N*np.linspace(0, 1, N)
    z = t*zmax/N # vertical position
    r = t*rmax/N # distance to origin, polar coordinates
    theta = t*2*pi*Nturns/N # angle, polar coordinates
    x = r*np.cos(theta)
    y = r*np.sin(theta)
    P = np.array([x,y,z]).T
    return P
    
    
    


class simrun(simsetup):
    def __init__(self):
        simsetup() # inherit atributes and methods from simsetup
        N = 500
        self.N = N # simulation data points
        self.PT = np.zeros((N,3)) # contains position of B frame
        self.RT = np.zeros((N,3,3)) # contains rotation matrix of B frame
        self.PTe = np.zeros((N,3)) # contains position estimated of B frame
        self.RTe = np.zeros((N,3,3)) # contains rotation matrix estimated of B frame
        self.PTerr = np.zeros((N,3)) # contains position errors of B frame
        self.RTerr = np.zeros((N,3,3)) # contains error rotation matrix R*Rest.T of B frame
        self.Eul = np.zeros((N,3)) # contains euler angles of B frame
        self.Eule = np.zeros((N,3)) # contains euler angles of B frame
        self.Eulerr = np.zeros((N,3)) # contains euler angles of B frame
        self.nA = 6 # number of beacons in frame A
        self.nB = 6 # number of beacons in frame B
        self.A = np.c_[np.zeros((3,1)),1.5*np.eye(3),np.random.randn(3,self.nA-4)]
        self.B = np.c_[np.zeros((3,1)),1.5*np.eye(3),np.random.randn(3,self.nB-4)]
        self.pA = np.zeros((3,1)) # origin
        self.RA = np.eye(3) # identity matrix
        self.pB = 100*np.random.randn(3,1) # origin
        w = 0.1*np.random.randn(3,1) # origin
        self.RB = expm(skew(w)) # identity matrix
        #pB = pA
        #RB = RA
        self.sigma = 0.03 # range error standard deviation
    
    
    def run(self):
        #PT = 100*np.random.randn(N,3) # contains N position of B frame
        N=self.N
        zmax = 10
        rmax = 50
        Nturns = 1
        self.PT = spiral_trajectory(N,zmax,rmax,Nturns)
        for i in range(N):
            w = 0.1*np.random.randn(3,1) # random axis 
            R = expm(skew(w)) # random rotation matrix 
            self.RT[i,:,:] = R # stores rotation matrix in 3D array
            self.Eul[i,:] = tf.euler_from_matrix(R,'rxyz') # euler angles
            self.pB = self.PT[i,:][:,np.newaxis] # current position of frame B in iteration i
            self.RB = R # current rotation matrix        
            Dm = self.get_Dm() # computes squared range measurements
            Rest,pest = self.repeater(Dm) # use repeater algorithm to compute pose
            self.PTe[i,:] = pest.flat
            self.PTerr[i,:] = self.PT[i,:]-pest.flat # position estimation error
            self.RTe[i,:,:] = Rest # stores estimated rotation matrix in 3D array
            self.RTerr[i,:,:] = R.dot(Rest.T) # stores estimated error rotation matrix in 3D array
            self.Eule[i,:] = tf.euler_from_matrix(Rest,'rxyz') # euler angles
            self.Eulerr[i,:] = tf.euler_from_matrix(R.dot(Rest.T),'rxyz') # euler angles
        
    def plot_run(self):    
        self.pB = 5*np.random.randn(3,1) # origin
        self.RB = np.eye(3)
        self.plot_simsetup()
    
        PT = self.PT 
        RT= self.RT
        PTe = self.PTe 
        RTe = self.RTe
        PTerr = self.PTerr 
        RTerr= self.RTerr
        Eul = self.Eul 
        Eule = self.Eule 
        Eulerr = self.Eulerr 
    
        fig = plt.figure(2)
        plt.clf()
        ax = fig.add_subplot(111, projection='3d')
        plot_frame(self.pA,self.RA)
        #plot_frame(S.pB,S.RB)
        plot_array(ax,self.A,self.pA,self.RA)
        #plot_array(ax,S.B,S.pB,S.RB)
        plt.plot(PT[:,0],PT[:,1],PT[:,2],'.')
        plt.axis('equal')
        plt.show()
        
        fig = plt.figure(3)
        plt.clf()
        plt.subplot(2, 1, 1)
        plt.plot(PT,'-')
        plt.plot(PTe,'.')
        plt.xlabel('Sample')
        plt.ylabel('Position [m]')
        plt.legend(['x','y','z'])
        plt.grid()
        plt.subplot(2, 1, 2)
        plt.plot(Eul*180/pi,'-')
        plt.plot(Eule*180/pi,'.')
        plt.xlabel('Sample')
        plt.ylabel('Euler angles  [deg]')
        plt.legend(['roll','pitch','yaw'])
        plt.grid()    
        plt.show()
        
        fig = plt.figure(4)
        plt.clf()
        plt.subplot(2, 1, 1)
        plt.plot(PTerr,'.-')
        plt.xlabel('Sample')
        plt.ylabel('Position Error [m]')
        plt.legend(['x','y','z'])
        plt.grid()
        plt.subplot(2, 1, 2)
        plt.plot(Eulerr*180/pi,'.-')
        plt.xlabel('Sample')
        plt.ylabel('Euler angles error [deg]')
        plt.legend(['roll','pitch','yaw'])
        plt.grid()    
        plt.show()  
        
#if __name__ == "__main__":
#S = simsetup();  
S = simrun()
#al, be, ga = tf.euler_from_matrix(S.RB, 'rxyz')
#N = 100
#zmax = 20
#rmax = 300
#Nturns = 1
#PT = spiral_trajectory(N,zmax,rmax,Nturns)

S.run()
S.plot_simsetup()
S.plot_run()

#fig = plt.figure(5)
#plt.clf()
#ax = fig.add_subplot(111, projection='3d')
#plot_frame(Sr.A,Sr.RA)
##plot_frame(S.pB,S.RB)
#plot_array(ax,Sr.A,Sr.pA,Sr.RA)
##plot_array(ax,S.B,S.pB,S.RB)
#plt.plot(PT[:,0],PT[:,1],PT[:,2],'.-')
#plt.axis('equal')
#plt.show()


w = 0.5*np.random.randn(3,1) # random axis 
R = expm(skew(w)) # random rotation matrix 
S.RB = R 
p = 10*np.random.randn(3,1) # origin
S.pB = p
S.sigma = 0.05
D = S.get_D()
Dm = S.get_Dm()
Rest,pest = S.repeater(Dm)
E = np.sqrt(Dm)-np.sqrt(D)


print(R)
print(Rest)
print(p)
print(pest)    
print(E)

# spiral position










    