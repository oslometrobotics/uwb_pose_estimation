import cProfile
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.linalg import expm
#import transformations as tf
from uwbpose import *
from test_gradient_descent import *

 


def profile_gradient_descent():
    N = 10
    S = simrun(N)
    zmax = 10
    rmax = 10
    Nturns = 2
    PT = spiral_trajectory(N,zmax,rmax,Nturns)
    w = np.array([0,0,1])
    w# = np.random.randn(3,1)
    s = 5
    RT = rotation_trajectory_axis(N,w,s)
    S.PT = PT # sets trajectory position
    S.RT = RT # sets trajectory rotation
    S.sigma = 0.1 # range error std
    #alg1 = repeater
    alg2 = gradient_descent # sets algorithm to use
    #S.run(alg1) # run algorithm
    S.run(alg2, S.sigma) # run algorithm
    plot_run(S) # plot results


cProfile.run('profile_gradient_descent()')
