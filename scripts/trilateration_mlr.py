#import matplotlib.pyplot as plt
import numpy as np
import pdb
#pdb.set_trace()

def get_ranges1(A,p):
    """Compute array of ranges between anchors and one tag
    A is 3xnA array of local beacon coordinates
    p is 3x1 array relative position vector from A to B expressed in A
    ranges is nA array of ideal ranges (without noise) 
    """
    nA = A.shape[1]
    ranges = np.zeros(nA) # initialize
    #pdb.set_trace()
    for i in range(nA):
        z = p - A[:,i] # distance vector
        ranges[i]= np.sqrt(np.square(z).sum()) 
    return ranges
          
def get_measured_ranges1(A,p,sigma):
    """compute range measurements with noise between 1 tag and set of anchors
    sigma is standard deviation of range measurements in cm
    Returns mranges, is nA array of range measurements with noise
    """
    nA = A.shape[1]
    ranges = get_ranges1(A,p) # compute matrix of square ranges
    mranges = ranges + sigma*np.random.randn(nA) #range measurements matrix         
    return mranges


def gradient_mlr1(A,p,mranges):
    """ Computes gradient of ML-R cost function for a set of anchors and one tag
    A is 3xnA array of local beacon coordinates
    p is 3x1 array tag position vector
    """
    ranges = get_ranges1(A,p)
    # check for very small ranges which would create divisio by zero later on
    rmin = 0.01  # minimum acceptable range
    if ranges.min() < rmin: # 
        print("warning one of the ranges is less that %f " % rmin )
        ranges[ranges<rmin] += rmin # regularization, avoid division by zero
                    
    G = -(p[:,np.newaxis]-A).dot((mranges-ranges)/ranges)        
    return G
 


def hessian_mlr1(A,p,mranges):
    """ Computes Hessian of ML-R cost function for a set of anchors and one tag
    A is 3xnA array of local beacon coordinates
    p is 3x1 array tag position vector
    """
    nA = A.shape[1]
    ranges = get_ranges1(A,p)
    # check for very small ranges which would create divisio by zero later on
    rmin = 0.01  # minimum acceptable range
    if ranges.min() < rmin: # 
        print("warning one of the ranges is less that %f " % rmin )
        ranges[ranges<rmin] += rmin # regularization, avoid division by zero
    C = p[:,np.newaxis]-A
    a = (mranges-ranges)
    H = np.dot(C/mranges/mranges, C.T)
    H += np.dot(C*np.power(ranges,-3)*a,C.T)
    H += -np.sum(a/ranges)*np.eye(3)
    return H
 
def armijo(A,p,mranges,d):
    """ Armijo line search
    A is 3xnA array of local beacon coordinates
    p is 3x1 array tag position vector
    mranges is nA array of measured ranges
    sigma is standard deviation
    d is 3 array line search direction
    """
    m_max = 30
    beta = 0.5
    ss = 0.1
    s = 1
    m =0
    G = gradient_mlr1(A,p,mranges)
    t = np.power(beta,m)*s
    pt = p + t*d;
    f0 = mlr1_cost(A,p,mranges)
    ft = mlr1_cost(A,pt,mranges)
    while m<=m_max and f0 < ft - t*ss*G.T.dot(d):
        m += 1
        t = np.power(beta,m)*s
        pt = p + t*d;
        ft = mlr1_cost(A,pt,mranges)
    return (pt,m,t)   
    
   
def mlr1_cost(A,p,mranges):
    """ Computes cost of Maximum likelihood with ranges (ML-R) cost function
    """
    ranges = get_ranges1(A,p)
    f = 0.5*np.nansum(np.square(mranges-ranges))
    return f

def trilaterateration_mlr1(A,mranges,p0=None,Nmax=100, eps = 1e-8, verbose=False):
    """ Computes position estimate based on a set of range measurements
    A is 3xnA array of local anchor coordinates
    mranges is nA array of range measurements
    mranges may contain nan values indicating missing/outlier data
    p0 is 3 array initial position estimate optional argument
    If p0 is not given then the algorithm uses the latest value returned by function. 
    That is it stores the result of the previous time the function was called and successfully terminated.
    If it is the first time that the function is called and no initial values provided, then origin is used for initialization
    Nmax is integer, maximum number of iterations
    eps is termination condition on the size of the gradient
    """    
    # Initialization
    # check if previous call resulted in succesful estimate
    if not hasattr(trilaterateration_mlr1, "p0") :
        trilaterateration_mlr1.p0 = np.random.randn(3)  # it doesn't exist yet, so initialize attribute
    p = trilaterateration_mlr1.p0 # use value from previous time function was executed
    # check if initial values explicitily given as arguments
    if p0 is not None:# p0 given as argument, use it instead as initial value
        p = p0 
    # Main loop
    for i in range(Nmax):     
        # Compue gradient
        g = gradient_mlr1(A,p,mranges)
        # check final condition
        e = np.sum(np.square(g))  # norm of gradient
        if np.sqrt(e) < eps: # terminal condition reached
            break     
        H = hessian_mlr1(A,p,mranges)
        d = -np.linalg.inv(H).dot(g)  # Newton direction
        # check descent condition
        if np.dot(d,g) >= 0 :  
            d = -g # if Newton direction is not descent then use negative gradient  
            if verbose:   
                print("warning, newton direction is not descent")
        # Determine step size Armijo in negative gradient direction
        (p,m,t) = armijo(A,p,mranges,d)
    if i == Nmax-1: # Solution is not reliable, exited with max iterations. 
        # Delete attribute p0
        # Next time gradient descent function is caled use repeater algorithm for initialization
        delattr(trilaterateration_mlr1,"p0")
    else: # converged before Nmax
        # Store result (R,p) so that it can be used as initial condition next time 
        # the function is called. Attribute is used as persistent variable.   
        trilaterateration_mlr1.p0 = p # 
    if verbose:      
        print("Newton descent: N=%i, e=%f " % (i, np.sqrt(e)))
    return p

          
