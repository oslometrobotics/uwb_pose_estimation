import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import rosbag_pandas
import os
import sys
import pandas as pd


currentdir = os.path.dirname(__file__)

#filename = os.path.join(currentdir,os.pardir,"data/experiment1.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment1_2.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment2.bag")
filename = os.path.join(currentdir,os.pardir,"data/experiment3.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment4.bag")
#filename = os.path.join(currentdir,os.pardir,"data/experiment5.bag")




#%%

print("importing rosbag file %s ..." % filename )
df = rosbag_pandas.bag_to_dataframe(filename)


#%%
C = list(df) # list of columns in rosbag file
Ctof = [s for s in C if s[0:3]  == "tof"] # extract columns with tof data

       
       

print("Removing outliers, remove values larger than 100m...")       
df[df.loc[:,Ctof].abs()>100] = np.nan # removes all values greater than 100, outliers

print("plotting...")   
df[Ctof].plot(style = '.-')
plt.grid()
plt.title("Original RAW TOF data")
plt.legend()

#%%
threshold = 0.3 # max allowed difference in meters between raw and median filtered
print("Removing outliers using median filter and threshold=%s ..." % threshold)      
dff = df.copy() # creates copy, filtered data
kw = dict(marker='o', linestyle='none', color='r', alpha=0.3)
fig, ax = plt.subplots() 
for c in Ctof:
    #c = Ctof[0]
    print("Removing outliers from %s" % c)      
    cmf = c+"mf" # new column with median filtered data
    df[cmf]= df[c].dropna().rolling(window=5,center=True).median().fillna(method='bfill').fillna(method='ffill')
    difference = np.abs(df[c]- df[cmf])
    outlier_idx = difference > threshold # contains index of outliers
    df[c].plot(style = '.-', ax = ax, label = c)
    #dfmf.plot(style = '.', ax = ax)
    df.ix[outlier_idx,c].plot( ax = ax,label = None, **kw)
    dff.ix[outlier_idx,c] = np.nan # remove outliers, replace with nan
plt.grid()
plt.title("Raw TOF data and detected outliers")
#%%
plt.legend()



#%%%

fig, ax = plt.subplots() 
dff[Ctof].plot(style = '.-')
plt.title("Filtered TOF data")
plt.legend()
plt.grid()

#%%
c = Ctof[0]
fig, ax = plt.subplots() 
df[c].plot(style = 'b.-',ax = ax)
df[c].dropna().diff().plot(style = 'g.-',ax = ax)




