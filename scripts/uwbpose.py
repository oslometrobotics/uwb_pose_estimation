import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.linalg import expm
from math import pi
import os
import sys
sys.path.append(os.getcwd())
import transformations as tf
import trilateration_mlr
import pdb


def skew(w):
    """returns a skew symmetric matric from a vector
    w is 3x1 array"""
    w1 = w[0]
    w2 = w[1]
    w3 = w[2]
    return np.array([[0,-w3,w2],[w3,0,-w1],[-w2,w1,0]])   

def plot_frame(R,p):
    """
    plots 3 vectors reference frame centered in p and orientation R
    p is a 3 array - origin of frame
    R is a 3x3 array - rotation matrix
    """
    #plt.quiver(p[0],p[1],p[2],R[0,0],R[1,0],R[2,0],color="k",linewidth=2) # X 
    plt.quiver(p[0]+R[0,0],p[1]+R[1,0],p[2]+R[2,0],R[0,0],R[1,0],R[2,0],color="k",linewidth=2) # X 
    plt.quiver(p[0]+R[0,1],p[1]+R[1,1],p[2]+R[2,1],R[0,1],R[1,1],R[2,1],color="b",linewidth=2) # X 
    plt.quiver(p[0]+R[0,2],p[1]+R[1,2],p[2]+R[2,2],R[0,2],R[1,2],R[2,2],color="g",linewidth=2) # X 

def plot_array(ax,A,R,p):
    """plots beacon array in matrix A in position p and orientation given by R
    ax is handle to matplotlib axes
    A is 3xnA array
    p is 3 array
    R is 3x3 array"""
    X = np.dot(R,A) + p[:,np.newaxis] # X contains beacon coordinates in "inertial" frame
    nA = A.shape[1] # number of columns
    ax.scatter(X[0,:],X[1,:],X[2,:],zdir='z', s=20) # plots beacon positions
    for i in range(nA):
        plt.plot([p[0], X[0,i]],[p[1],  X[1,i]],[p[2], X[2,i]],linewidth=2)
           
def plot_simsetup(A,B,R,p):
    fig = plt.figure()
    #plt.hold(True)
    ax = fig.add_subplot(111, projection='3d')
    RA = np.eye(3)
    pA = np.zeros(3)
    #plot_frame(RA,pA)
    #plot_frame(R,p)
    plot_array(ax,A,RA,pA)
    plot_array(ax,B,R,p)
    plt.axis('equal')
    plt.show()

        
def get_D(A,B,R,p):
    """Compute matrix of square ranges
    A is 3xnA array of local beacon coordinates
    B is 3xnB array of local beacon coordinates  
    R is 3x3 array containin rotation matrix from A to B expressed in A
    p is 3x1 array relative position vector from A to B expressed in A
    D is nAxnB array of squared ranges 
    """
    nA = A.shape[1]
    nB = B.shape[1]
    D = np.zeros((nA,nB)) # initialize
    #pdb.set_trace()
    for i in range(nA):
        for j in range(nB):
            z = R.dot(B[:,j]) +p -A[:,i] # distance vector
            # print(z)
            D[i,j]= np.dot(z.T,z) 
    return D
    
def get_Dm(A,B,R,p,sigma):
    """compute measurements with noise
    D is nAxnB array of squared ranges
    sigma is standard deviation of range measurements in cm
    Returns Dm, is nAxnB array of squared range measurements with noise
    """
    nA = A.shape[1]
    nB = B.shape[1]
    D = get_D(A,B,R,p) # compute matrix of square ranges
    r = np.sqrt(D) # square root, ideal range  matrix
    rm = r + sigma*np.random.randn(nA,nB) #range measurements matrix         
    Dm = rm*rm
    return Dm

def get_ranges(A,B,R,p):
    """Compute matrix ofranges
    A is 3xnA array of local beacon coordinates
    B is 3xnB array of local beacon coordinates  
    R is 3x3 array containin rotation matrix from A to B expressed in A
    p is 3 array relative position vector from A to B expressed in A
    ranges is nAxnB array of ideal ranges (without noise) 
    """
    nA = A.shape[1]
    nB = B.shape[1]
    ranges = np.zeros((nA,nB)) # initialize
    #pdb.set_trace()
    for i in range(nA):
        for j in range(nB):
            z = R.dot(B[:,j]) +p -A[:,i] # distance vector
            ranges[i,j]= np.sqrt(np.square(z).sum()) # norm of vector
    return ranges


def get_ranges1(A,p):
    """Compute array of ranges between anchors and one tag
    A is 3xnA array of local beacon coordinates
    p is 3x1 array relative position vector from A to B expressed in A
    ranges is nA array of ideal ranges (without noise) 
    """
    nA = A.shape[1]
    ranges = np.zeros(nA) # initialize
    #pdb.set_trace()
    for i in range(nA):
        z = p - A[:,i] # distance vector
        ranges[i]= np.sqrt(np.square(z).sum()) 
    return ranges
    
def get_measured_ranges(A,B,R,p,sigma):
    """compute range measurements with noise
    sigma is standard deviation of range measurements in cm
    Returns mranges, is nAxnB array of range measurements with noise
    """
    nA = A.shape[1]
    nB = B.shape[1]
    ranges = get_ranges(A,B,R,p) # compute matrix of square ranges
    mranges = ranges + sigma*np.random.randn(nA,nB) #range measurements matrix         
    return mranges
        
def get_measured_ranges1(A,p,sigma):
    """compute range measurements with noise between 1 tag and set of anchors
    sigma is standard deviation of range measurements in cm
    Returns mranges, is nA array of range measurements with noise
    """
    nA = A.shape[1]
    ranges = get_ranges1(A,p) # compute matrix of square ranges
    mranges = ranges + sigma*np.random.randn(nA) #range measurements matrix         
    return mranges

def repeater_full(A,B,mranges):
     """ RangE only PosE estimAtion using TrilatERation-procustes 
     Algorithm assumes that mranges is full, there is no missing data (nan)
     A is 3xnA array of local beacon coordinates
     B is 3xnB array of local beacon coordinates  
     mranges is nAxnB array of range measurements
     returns estimated position and rotation matrix (Rest,pest)
     Rest is 3x3 array containin rotation matrix from A to B expressed in A
     pest is 3x1 array relative position vector from A to B expressed in A
     """
     nA = A.shape[1]
     nB = B.shape[1]
     Dm = mranges*mranges # matrix of squared range measurements
     Ma = np.eye(nA)-1./float(nA)*np.ones((nA,nA))
     Mb = np.eye(nB)-1./float(nB)*np.ones((nB,nB))
     Ac = A.dot(Ma)
     T = 1./2*np.linalg.inv(Ac.dot(Ac.T)).dot(Ac)
     Ba = T.dot(np.tile(np.diagonal(A.T.dot(A)),(nB,1)).T-Dm)
     G = Ba.dot(Mb).dot(B.T)
     U,s,V = np.linalg.svd(G,full_matrices=1)
     q = np.linalg.det(U.dot(V))
     Rest = U.dot(np.diag([1,1,q])).dot(V)
     pest = 1./float(nB)*(Ba-Rest.dot(B)).dot(np.ones(nB))
     return (Rest,pest)

def repeater(A,B,mranges, trilateration="mlr" ):
     """ RangE only PosE estimAtion using TrilatERation-procustes 
     A is 3xnA array of local beacon coordinates
     B is 3xnB array of local beacon coordinates  
     mranges is nAxnB array of range measurements
     mranges may contain nan values indicating missing/outlier data
     trilateration = "mlr", "ls" indicates which trilateration algorithm to use
     "mlr" is Newton descent method applied to the maximum likelihood function
     "ls" is a least squares solution based on squaring range measurements and applying a centering operator
     returns estimated position and rotation matrix (Rest,pest)
     Rest is 3x3 array containin rotation matrix from A to B expressed in A
     pest is 3x1 array relative position vector from A to B expressed in A
     """
     nB = B.shape[1]
     Ba = np.zeros((3,nB))
     idx = [] # contains indexes for which tag position can be computed
     for j in range(nB):
         if trilateration == "ls":
             baj = trilateration_ls(A,mranges[:,j])
         else: # use default trilateration == "mlr":
              baj = trilateration_mlr.trilaterateration_mlr1(A,mranges[:,j],Nmax=100, eps = 1e-8)
         # if there were not enough valid ranges returns nan
         Ba[:,j] = baj        
         if np.isnan(baj).sum() == 0: # baj dont have nan values
            idx.append(j)
     Ba = Ba[:,idx] # extract columns for which there is valid data
     B = B[:,idx] # extract columns for which there is valid data
     nB = B.shape[1]
     Mb = np.eye(nB)-1./float(nB)*np.ones((nB,nB))
     G = Ba.dot(Mb).dot(B.T)
     U,s,V = np.linalg.svd(G,full_matrices=1)
     q = np.linalg.det(U.dot(V))
     Rest = U.dot(np.diag([1,1,q])).dot(V)
     pest = 1./float(nB)*(Ba-Rest.dot(B)).dot(np.ones(nB))
     return (Rest,pest)
 
def trilateration_ls(A,mranges):
    """ Trilateration using least squares 
    A is 3xnA array of local anchor coordinates
    mranges is nA array of range measurements
    mranges may contain nan values indicating missing/outlier data
    returns estimated position pest
    pest is 3x1 array relative position vector from A to p expressed in A
    """
    idx = ~np.isnan(mranges) # indexes of ranges than contain valid data
    if len(idx) < 4: # not enough ranges
        pest = np.nan*np.ones(3) # returns nan        
        return pest
    mranges = mranges[idx] # extract valid data
    A = A[:,idx] # extract anchors for which data is available
    nA = A.shape[1] # number of anchors with valid data           
    dm = mranges*mranges # array of squared range measurements
    M = np.eye(nA)-1./float(nA)*np.ones((nA,nA)) # centering matrix with vector of ones on its null space
    Ac = A.dot(M) # centered anchor coordinates
    T = 1./2*np.linalg.inv(Ac.dot(Ac.T)).dot(Ac) # presudo inverse, used to solve least squares
    c = np.square(A).sum(0) # contains square norm of anchor positions
    pest = T.dot(c-dm)
    return pest

    
 
def spiral_trajectory(N,zmax,rmax,Nturns):
   # computes spiral trajectory trajectory = [x y z] is a array N x 3 
    t = N*np.linspace(0, 1, N) # time vector
    z = t*zmax/N + 0.5 # vertical position, z coordinate
    r = t*rmax/N + 0.5 # distance to origin, polar coordinates
    theta = t*2*pi*Nturns/N # angle, polar coordinates
    x = r*np.cos(theta) # x coordinate
    y = r*np.sin(theta) # y coordinte
    trajectory = np.array([x,y,z]).T
    return trajectory

def rotation_trajectory_axis(N,w=None,s=None):
    """ Generates trajectory of rotation matrices linear along an axis
    Returns a Nx3x3 array 
    N is the number of points
    w is the rotation axis 3x1 array
    s is a scaling parameter. Small values will produce slower rotations
    """
    if w is None:
        w = np.array([1,1,1])
    if s is None:
        s = 1.0
    RT = np.zeros((N,3,3)) # contains rotation matrix of B frame
    for i in range(N): 
        R = expm(s*float(i)/float(N)*skew(w)) # rotation matrix
        RT[i,:,:] = R # store rotation matrix
    return RT
    #w = 1*np.random.randn(3,1) # random vector
    #self.RB = expm(skew(w)) # random rotation matrix
    
def rotation_trajectory_random(N,s):
    """ Generates trajectory of random rotation matrices
    Returns a Nx3x3 array 
    Each element is a random rotation matrix
    N is the number of points
    s is a scaling parameter. Small values will produce rotation matrices near to identity matrix
    """
    RT = np.zeros((N,3,3)) # contains rotation matrix of B frame
    for i in range(N): 
        w = s*np.random.randn(3) # random vector
        R = expm(skew(w)) # random rotation matrix
        RT[i,:,:] = R # store rotation matrix
    return RT

def antenna_array(n,d):
    """compute array beacon local coordintes matrix
    n is the number of beacons
    d is nominal distance between beacons
    returns is 3xn array of coordinates
    The first 4 are in the vertices of a orthogonal set of axis with distance d
    The remaining beacons 
    n >= 4
    """
    A = np.array([[0,1,0,0,1],
                  [0,0,1,0,1],
                  [0,0,0,1,0]])
    if n > 5:
        A = np.c_[A, np.random.randn(3,n-5)]
    return d*A[:,0:n]

def mlr_gradient(A,B,R,p,mranges):
    """ Computes gradient of ML-R cost function
    A is 3xnA array of local beacon coordinates
    B is 3xnB array of local beacon coordinates  
    p is 3 array, the estimated position where gradient is to be evaluated
    mranges is nAxnB array of range measurements
    mranges may contain nan values indicating missing/outlier data
    returns gradient (G,v)  
    G is a 3x3 array (a tangent vector in so(3))  
    v is a 3 array
    """
    nA = A.shape[1]
    nB = B.shape[1]
    ranges = get_ranges(A,B,R,p) # compute expected ranges with pose (R,p)
    G = np.zeros((3,3)) # initialize
    v = np.zeros(3) # initialize
    for i in range(nA):
        for j in range(nB):
            if ~np.isnan(mranges[i,j]): # check if valid measurement available
                #pdb.set_trace()
                rij = ranges[i,j]
                mrij = mranges[i,j]
                bj = B[:,j] # tag
                ai = A[:,i] # anchor 
                cij = R.dot(bj) + p - ai # 3 array vector from anchor to tag
                G += -(mrij-rij)/rij*(np.outer(cij,bj) - R.dot(np.outer(bj,cij)).dot(R))
                v += -(mrij-rij)/rij*cij         
    return (G,v)                   

def mlr_cost(A,B,R,p,mranges):
    """ Computes value of ML-R cost function
    Maximum Likelihood with Ranges cost function
    A is 3xnA array of local beacon coordinates
    B is 3xnB array of local beacon coordinates  
    p is 3 array, the estimated position where gradient is to be evaluated
    mranges is nAxnB array of range measurements
    mranges may contain nan values indicating missing/outlier data
    """
    ranges = get_ranges(A,B,R,p) # 
    f = 0.5*np.nansum(np.square(mranges-ranges))
    return f

def geodesic(R,p,G,v,t):
    """ Computes geodesic in the special Eucliden group SE(3)
    That is the rotation matrix and position in the direction given
    by tangent vector (G,v) with step value t
    """
    Rt = R.dot(expm(t*R.T.dot(G)))
    pt = p + t*v
    return (Rt,pt)
    
def armijo(A,B,R,p,mranges,G,v, plot = False):
    """ Determines approximately optimal step size using Armijo rule
    """
    # default values
    #b = 0.2 # beta
    #s = 1.0 # s
    #ss = 0.1 # sigma
    b = 0.2 # beta
    s = 5.0 # s
    ss = 0.1 # sigma 
    m_max = 30
    def armijo_plot():
        """ Plot value of cost function along negative gradient descent geodesic 
        and step size selected by armijo algorithm
        """
        N = 200 # number fo points
        F = np.zeros(N) # array to store mlr cost function values
        T = np.linspace(-5*t,10*t,N) # step size index        
        for i in range(N):
            (Rti,pti) = geodesic(R,p,G,v,T[i]) # geodesic step size t[i]
            F[i] = mlr_cost(A,B,Rti,pti,mranges)
        F1 = f0 - T*e # first order aproximation of mlr cost function
        F2 = f0 - ss*T*e # first order aproximation of mlr cost function
        plt.figure(1)
        plt.plot(T,F, "b-", label = "Geodesic line search")
        plt.plot(T,F1, "k-.", label = "First order approx.")
        plt.plot(T,F2, "g-.", label = "Armijo condition")
        plt.plot(t,ft,"ko", label = "Armijo step size")
        plt.plot(0,f0,"bo", label = "Initial")
        plt.grid()
        plt.xlabel("step size t")
        plt.ylabel("ML-R cost function")
        plt.legend()
        plt.title("Armijo line search at current iteration")
        #raw_input("Press ENTER to continue")
        print("Armijo converged in m = %i iterations" % m)
    
    f0 = mlr_cost(A,B,R,p,mranges)
    e = np.square(G).sum() + np.square(v).sum() # Norm of gradient squared 
    for m in range(m_max):
        t = np.power(b,m)*s # step size
        (Rt,pt) = geodesic(R,p,G,v,t)
        ft = mlr_cost(A,B,Rt,pt,mranges) 
        if ft <= f0 - ss*t*e: # sucess condition
        # step size yields sufficiently good cost function improvement
            if plot:    
                armijo_plot() # plots armijo line search   
            return (Rt,pt)  # sucessful return           
            # break    
    # if this is executed then m_max iterations reached without finding a solution
    print("Armijo line search failed. Max iterations %i reached" % m_max) 
    tk = np.power(b,m)*s # step size
    if plot:    
        armijo_plot() # plots armijo line search   
    return (Rt,pt)             

def gradient_descent(A,B,mranges,R0=None,p0=None,Nmax=150, eps = 1e-4, verbose=True):
    """ Computes pose estimate based on a set of range measurements
    A is 3xnA array of local beacon coordinates
    B is 3xnB array of local beacon coordinates  
    mranges is nAxnB array of range measurements
    mranges may contain nan values indicating missing/outlier data
    R0 is 3x3 array initial rotation matrix estimate. 
    p0 is 3 array initial position estimate
    If R0,or p0, are not given then the algorithm uses the latest value returned by function. 
    That is it stores the result of the previous time the function was called.
    If it is the first time that the function is called and no initial values provided, then the repeater algorithm is used for initialization
    Nmax is integer, maximum number of iterations
    eps is termination condition on the size of the gradient
    plots provides option to visualize internal line search at each iteration
    """    
    # Initialization
    # check if previous call resulted in succesful estimate
    if not hasattr(gradient_descent, "R0") or not hasattr(gradient_descent, "p0") :
        # create R0,p0 persistent attributes 
        #print("initialize using repeater")
        (R0,p0) = repeater(A,B,mranges) # use three step repeater algorithm
        gradient_descent.R0 = R0  # it doesn't exist yet, so initialize attribute
        gradient_descent.p0 = p0  # it doesn't exist yet, so initialize attribute
    R = gradient_descent.R0 # use value from previous time function was executed
    p = gradient_descent.p0 # use value from previous time function was executed
    # check if initial values explicitily given as arguments
    if R0 is not None: # R0 given as argument, use it instead as initial value
        R = R0 
    if p0 is not None:# p0 given as argument, use it instead as initial value
        p = p0 
    # Main loop
    for i in range(Nmax):     
        # Compue gradient
        (G,v) = mlr_gradient(A,B,R,p,mranges)
        # check final condition
        e = np.sum(np.square(G)) + np.sum(np.square(v)) # norm of gradient
        if np.sqrt(e) < eps: # terminal condition reached
            break     
        # Determine step size Armijo in negative gradient direction
        (R,p) = armijo(A,B,R,p,mranges,-G,-v)
        #(R,p) = armijo(A,B,R,p,mranges,sigma,-G,-v, plot = True)
        #(R,p) = geodesic(R,p,-G,-v,1.0) # debug contant step size
    if i == Nmax-1: # Solution is not reliable, exited with max iterations. 
        # Delete attributes R0 and p0
        # Next time gradient descent function is caled use repeater algorithm for initialization
        delattr(gradient_descent,"R0")
        delattr(gradient_descent,"p0")
    else: # converged before Nmax
        # Store result (R,p) so that it can be used as initial condition next time 
        # the function is called. Attribute is used as persistent variable.   
        gradient_descent.R0 = R # 
        gradient_descent.p0 = p # 
    if verbose:      
        print("Gradient descent: N=%i, e=%f " % (i, np.sqrt(e)))
    return (R,p)




class simrun():
    def __init__(self,N=100,nA=5,nB=5):
        #simsetup() # inherit atributes and methods from simsetup
        #N = 500
        self.N = N # simulation data points
        #nA = 5 # number of beacons in vehicle A, default
        #nB = 5 # number of beacons in vehicle B, default
        self.A = antenna_array(nA,1.5) # vehicle A 1.5 meters distance between beacons
        self.B = antenna_array(nB,0.5) # vehicle B 0.5m distance between beacons
        self.pA = np.zeros(3) # origin
        self.RA = np.eye(3) # identity matrix
        self.pB = 4*np.random.randn(3) # initialization, random position
        self.RB = np.eye(3) # identity matrix
        self.sigma = 0.01 # range error standard deviation
        self.PT = np.zeros((N,3)) # contains position of B frame
        self.RT = np.zeros((N,3,3)) # contains rotation matrix of B frame
        self.PTe = np.zeros((N,3)) # contains position estimated of B frame
        self.RTe = np.zeros((N,3,3)) # contains rotation matrix estimated of B frame
        self.PTerr = np.zeros((N,3)) # contains position errors of B frame
        self.RTerr = np.zeros((N,3,3)) # contains error rotation matrix R*Rest.T of B frame
        self.Eul = np.zeros((N,3)) # contains euler angles of B frame
        self.Eule = np.zeros((N,3)) # contains euler angles of B frame
        self.Eulerr = np.zeros((N,3)) # contains euler angles of B frame
        self.set_mranges() 
        
    def nA(self):
        nA = self.A.shape[1]
        return nA
    def nB(self):
        nB = self.B.shape[1] 
        return nB

    def set_mranges(self):
        self.mranges = np.zeros((self.N,self.nA(),self.nB())) # initialize
        for i in range(self.N):
            R = self.RT[i,:,:] # curret iteration rotation matrix
            p = self.PT[i,:] # current position of frame B in iteration i
            self.mranges[i,:] = get_measured_ranges(self.A,self.B,R,p,self.sigma)       
    
    def run(self,alg,*args):
        """ Computes pose estimates of a simulation run using algorithm alg
        alg is an algorithm in the form alg(A,B,mranges,*args) and returns a pair (Rest,pest)
        args contains optional arguments for algorithm
        """        
        for i in range(self.N):       
            R = self.RT[i,:,:] # curret iteration rotation matrix
            p = self.PT[i,:] # current position of frame B in iteration i
            self.Eul[i,:] = tf.euler_from_matrix(R,'rxyz') # euler angles
            self.RB = R # current rotation matrix        
            mranges = self.mranges[i,:]
            Rest,pest = alg(self.A,self.B,mranges,*args) # use algorithm to compute pose
            self.PTe[i,:] = pest
            self.PTerr[i,:] = self.PT[i,:]-pest # position estimation error
            self.RTe[i,:,:] = Rest # stores estimated rotation matrix in 3D array
            self.RTerr[i,:,:] = R.dot(Rest.T) # stores estimated error rotation matrix in 3D array
            self.Eule[i,:] = tf.euler_from_matrix(Rest,'rxyz') # euler angles
            self.Eulerr[i,:] = tf.euler_from_matrix(R.dot(Rest.T),'rxyz') # euler angles
            print("iteration %i out of %i" % (i,self.N))
        
def plot_run(S):
    """ Plots data from simulation run S
    """
    # Unpack data in simulation run S
    A = S.A
    B = S.B
    RA = S.RA
    pA = S.pA
    PT = S.PT 
    RT = S.RT
    #RT= S.RT
    PTe = S.PTe 
    #RTe = S.RTe
    PTerr = S.PTerr 
    #RTerr= S.RTerr
    Eul = S.Eul 
    Eule = S.Eule 
    Eulerr = S.Eulerr   
   
    # Plot setup
    #p = 2*np.ones(3) # places B in [4,4,4]
    p = np.array([3,3,4])
    R = np.eye(3) # identity matrix rotation
    plot_simsetup(A,B,R,p)
    plot_simsetup(S.A,S.B,R,p)
    ax = plt.gca()
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
    ax.set_zlabel("z [m]")
    
    # Plot trajectory
    fig = plt.figure(2)
    plt.clf()
    ax = fig.add_subplot(111, projection='3d')
    #plot_frame(RA,pA)
    plot_array(ax,A,RA,pA)
    plt.plot(PT[:,0],PT[:,1],PT[:,2],'-')
    #plot_frame(RA,PT[-1,:])
    Np = 12 # number of times robot B is plotted
    N = S.N
    step = int(np.floor(N/Np))
    for i in range(Np)[1:]:
        idx = step*i
        plot_array(ax,B,RT[idx,:],PT[idx,:])
    plot_array(ax,B,RT[-1,:],PT[-1,:])
    plt.axis('equal')
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
    ax.set_zlabel("z [m]")
    plt.show()
    
    # Plot actual and estimated pose
    fig = plt.figure(3)
    plt.clf()
    plt.subplot(2, 1, 1)
    plt.plot(PTe,'.')
    plt.plot(PT,'-')
    plt.xlabel('Sample')
    plt.ylabel('Position [m]')
    plt.legend(['x','y','z'])
    plt.grid()
    plt.subplot(2, 1, 2)
    plt.plot(Eule*180/pi,'.')
    plt.plot(Eul*180/pi,'-')
    plt.xlabel('Sample')
    plt.ylabel('Euler angles  [deg]')
    plt.legend(['roll','pitch','yaw'])
    plt.grid()    
    plt.show()
    
    # Plot pose estimation errors
    fig = plt.figure(4)
    plt.clf()
    plt.subplot(2, 1, 1)
    plt.plot(PTerr,'.')
    plt.xlabel('Sample')
    plt.ylabel('Position Error [m]')
    plt.legend(['x','y','z'])
    plt.grid()
    plt.subplot(2, 1, 2)
    plt.plot(Eulerr*180/pi,'.')
    plt.xlabel('Sample')
    plt.ylabel('Euler angles error [deg]')
    plt.legend(['roll','pitch','yaw'])
    plt.grid()    
    plt.show()  









    