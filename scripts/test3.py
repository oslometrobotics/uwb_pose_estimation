
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
#from scipy.linalg import expm
# import pdb
#from math import pi
#import os
#import sys
#sys.path.append(os.getcwd())
#import transformations as tf

import uwbpose



N = 100
S = uwbpose.simrun(N)
zmax = 10
rmax = 10
Nturns = 2
PT = uwbpose.spiral_trajectory(N,zmax,rmax,Nturns)
w = np.array([0,0,1])
w# = np.random.randn(3,1)
s = 5
RT = uwbpose.rotation_trajectory_axis(N,w,s)
S.PT = PT # sets trajectory position
S.RT = RT # sets trajectory rotation
S.sigma = 0.04 # range error std
alg1 = uwbpose.repeater # sets algorithm to use
S.run(alg1) # run algorithm
uwbpose.plot_run(S) # plot results


