%function [p,varagout] = MLR_newton(P,R,rm,p0,varargin)
function [p,ITERDATA] = MLR_newton(P,R,rm,p0,epsilon,NMAXITER)
 
 
n = size(P,1);
m = size(P,2);
 
 
 
 
default_NMAXITER = 25;
default_epsilon = 1e-6;
 
if nargin==3
    p0 =zeros(n,1);
    NMAXITER = default_NMAXITER;
    epsilon = default_epsilon;
elseif nargin ==4
    NMAXITER = default_NMAXITER;
    epsilon = default_epsilon;
end
 
p = p0 ;
G = get_gradient(P,R,rm,p);
 
 
SAVE_ITERDATA = 0;
if nargout > 1 % output iteration data
    SAVE_ITERDATA = 1;
 
    PT = zeros(n,NMAXITER);   % estimated position
    GT = zeros(NMAXITER,1);   % gradient
    MLRT = zeros(NMAXITER,1);   % MLR cost function value
    APT = zeros(NMAXITER,2);   % [tk mk] armijo parameters
    ITYPE = cell(NMAXITER,1); % iteration type 'gradient' or 'newton'
 
    PT(:,1) = p;   % position
    GT(1) = norm(G);   % gradient
    MLRT(1) = likelihood(P,R,rm,p);   % MLR cost function value
 
end
 
 
 
%epsilon = 1e-3;
k=2;
%for k=1:Nmaxiter
while k<=NMAXITER && norm(G)>=epsilon % & abs(lk-lk_old) > epsilon_incr
 
    G = get_gradient(P,R,rm,p);
    H = get_hessian(P,R,rm,p);
 
    dN = -inv(H)*G;
    if dN'*G >= 0 % check descent condition
        dk = -G;
    else
        dk = dN;
    end
    %dk = -G;
    
    [p,mk,sk] = armijo_search(P,R,rm,p,dk);
     
    %disp(num2str(x'));
 
    if SAVE_ITERDATA
 
 
        PT(:,k) = p;   % position
        GT(k) = norm(G);   % gradient
        MLRT(k) = likelihood(P,R,rm,p);   % MLR cost function value
        APT(k-1,:) = [mk sk]; % armijo parameters
        if dk == dN
            ITYPE{k} = 'Newton';
        elseif dk == -G
            ITYPE{k} = 'gradient';
        end
 
    end
 
 
    k=k+1;
 
end
 
 
if SAVE_ITERDATA
 
    ITERDATA.PT = PT(:,1:k-1);
    ITERDATA.GT = GT(1:k-1);
    ITERDATA.MLRT = MLRT(1:k-1);
    ITERDATA.APT = APT(1:k-1,:);
    ITERDATA.ITYPE =ITYPE(1:k-1);
 
end
 
 
 
 
 
 
 
 
 
 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r = get_r(P,p)
m = size(P,2);
 
r = sqrt(diag(P'*P) + repmat(p'*p,m,1) - 2*P'*p);
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function G = get_gradient(P,R,rm,p)
m = size(P,2);
n = size(P,1);
 
r = get_r(P,p);
C = repmat(p,1,m) - P;
G = -C*inv(diag(r))*inv(R)*(rm-r);
 
 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function H = get_hessian(P,R,rm,p)
m = size(P,2);
n = size(P,1);
 
%keyboard
 
r = get_r(P,p);
C = repmat(p,1,m) - P;
D = diag(r);
 
%H = C*inv(diag(r))*(inv(R) - diag((inv(R)*(rm-r))./r))*inv(diag(r))*C';
a = inv(R)*(rm-r);
 
%H = C*inv(D)*inv(R)*inv(D)*C' + C*D^(-3)*diag(a)*C' - sum(a./r)*eye(n);
%H = C*inv(diag(r))*(inv(R) - diag((inv(R)*(rm-r))./r))*inv(diag(r))*C';
 
% % apparently works...
% H1 = C*inv(D)*inv(R)*inv(D)*C';
% H2 = zeros(n);
% for i=1:m
%     H2 = H2 + a(i)*(1/r(i)*eye(n) - 1/r(i)^3*(p-P(:,i))*(p-P(:,i))');
% end
% H = H1 - H2;
 
H = C*inv(D)*inv(R)*inv(D)*C' + C*D^(-3)*diag(a)*C' - sum(a./r)*eye(n);
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pk,mk,sk] = armijo_search(P,R,rm,p,dk)
m = size(P,2);
n = size(P,1);
 
nmaxiter = 50;
beta = 0.5; % 0.2
sigma = 0.1; % sigma = 0.1;
s = 1;
%s = 2;
mk=0;
 
G = get_gradient(P,R,rm,p);
 
pk = p + beta^mk*s*dk;
 
while mk<=nmaxiter && likelihood(P,R,rm,p) < likelihood(P,R,rm,pk) - beta^mk*s*sigma*G'*dk
     
    mk = mk+1;
    pk = p + beta^mk*s*dk; % true geodesic
end 
sk = beta^mk*s; % step size
 
%returns xk
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function f = likelihood(P,R,rm,p)
n = size(P,1);
m = size(P,2);
 
r = get_r(P,p);
f = 0.5*(rm-r)'*inv(R)*(rm-r);
