import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.linalg import expm
#import transformations as tf
from uwbpose import *
import pdb

import copy


def print_figures(str):
    plt.figure(1)
    plt.savefig("figures/simsetup1.eps", format='eps', dpi=1000)
    plt.figure(2)
    plt.savefig("figures/simsetup2.eps", format='eps', dpi=1000)
    plt.figure(3)
    plt.savefig("figures/"+str+".eps", format='eps', dpi=1000)
    plt.figure(4)
    plt.savefig("figures/"+str+"_err.eps", format='eps', dpi=1000)


#        
###%%
##
#
#N = 10
#S = simrun(N)
#A = S.A
#B = S.B
#R = np.eye(3)
#p = np.zeros(3)
#w =  np.random.randn(3)
#Rest = expm(skew(w))
#pest = 2*np.random.randn(3)
#sigma = S.sigma
#mranges = get_measured_ranges(A,B,R,p,sigma)
#(R,p) = gradient_descent(A,B,mranges,R0=None,p0=None,Nmax=100, eps = 1e-5)
##(G,v) = mlr_gradient(A,B,Rest,pest,mranges,sigma)
##(R,p) = armijo(A,B,Rest,pest,mranges,sigma,-G,-v, plot = True)
#


#%%

N = 500
S = simrun(N)
zmax = 10
rmax = 10
Nturns = 1
PT = spiral_trajectory(N,zmax,rmax,Nturns)
w = 0.8*np.array([-1.0,0.5,1.5])
#w = np.random.randn(3,1)
s = 2
RT = rotation_trajectory_axis(N,w,s)
S.PT = PT # sets trajectory position
S.RT = RT # sets trajectory rotation
S.A = antenna_array(5,1.5)
S.B = antenna_array(5,0.5)
S.sigma = 0.03 # range error std
S.set_mranges()

#%%
alg1 = repeater
alg2 = gradient_descent # sets algorithm to use

S2 = copy.deepcopy(S)
#S3 = copy.deepcopy(S)

#%%


#S.run(alg1, "ls") # run algorithm
#S.run(alg1, "mlr") # run algorithm
#S2.run(alg1, "ls") # run algorithm
S.run(alg2) # run algorithm

#%%
plot_run(S) # plot results
#plot_run(S2)





#%%        

