import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.linalg import expm
#import transformations as tf
from uwbpose import *
import pdb




#        
###%%
##
#
#N = 10
#S = simrun(N)
#A = S.A
#B = S.B
#R = np.eye(3)
#p = np.zeros(3)
#w =  np.random.randn(3)
#Rest = expm(skew(w))
#pest = 2*np.random.randn(3)
#sigma = S.sigma
#mranges = get_measured_ranges(A,B,R,p,sigma)
#(R,p) = gradient_descent(A,B,mranges,R0=None,p0=None,Nmax=100, eps = 1e-5)
##(G,v) = mlr_gradient(A,B,Rest,pest,mranges,sigma)
##(R,p) = armijo(A,B,Rest,pest,mranges,sigma,-G,-v, plot = True)
#


##%%

N = 50
S = simrun(N)
zmax = 10
rmax = 10
Nturns = 1
PT = spiral_trajectory(N,zmax,rmax,Nturns)
# w = np.array([0,0,1])
w = np.random.randn(3,1)
s = 2
RT = rotation_trajectory_axis(N,w,s)
S.PT = PT # sets trajectory position
S.RT = RT # sets trajectory rotation
S.A = antenna_array(4,1.0)
S.B = antenna_array(4,0.5)
S.sigma = 0.05 # range error std
alg1 = repeater
alg2 = gradient_descent # sets algorithm to use
S.run(alg1) # run algorithm
#S.run(alg2, S.sigma) # run algorithm
plot_run(S) # plot results

