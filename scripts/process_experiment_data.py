import matplotlib.pyplot as plt
import numpy as np
import os
#import sys
import pandas as pd
# from helper_funcs import *

def print_df_size(df, df_name):
    print("{0}  -  Columns: {1}  -  Rows: {2}".format(df_name, len(df.columns), len(df)))

currentdir = os.getcwd()
for experiment_idx in ["6_" + str(subindex + 1) for subindex in range(7)]:


    pkl_filename = os.path.join(currentdir, os.pardir, "data/dff_experiment{0}.pkl".format(experiment_idx))
    dff = pd.read_pickle(pkl_filename)
    anchor_cols = [var for var in list(dff) if "anchor" in var]
    df = dff.ix[:,anchor_cols]  # We keep only the columns we are interested in
    print_df_size(df, "Imported data experiment " + experiment_idx)

    df.dropna(how="all")
    print_df_size(df, "After dropping NaN rows")


    # Create empty summary DataFrames
    summary_columns = ["Anchor {0}".format(idx) for idx in range(1, 6)]
    summary_index = ["Tag {0}".format(idx) for idx in range(1, 6)]
    df_mean = pd.DataFrame(index=summary_index, columns=summary_columns)
    df_std = pd.DataFrame(index=summary_index, columns=summary_columns)
    df_rel_error = pd.DataFrame(index=summary_index, columns=summary_columns)

    # Quickly visualize clean raw data
    plot = False  # Warning! Shows 25 consecutive (potentially annoying) plots
    for idx, col in enumerate(anchor_cols):
        anchor_num, tag_num = int(col[9]), int(col[20])
        ds_clean = df.loc[:, col].dropna()  # Remove NaN values
        ds_clean.index = ds_clean.index - ds_clean.index[0]  # Set start time to zero
        mean = ds_clean.mean()
        std = ds_clean.std()
        df_mean.ix["Tag {0}".format(tag_num), "Anchor {0}".format(anchor_num)] = mean
        df_std.ix["Tag {0}".format(tag_num), "Anchor {0}".format(anchor_num)] = std
        df_rel_error.ix["Tag {0}".format(tag_num), "Anchor {0}".format(anchor_num)] = std/mean

        if plot:
            fig, ax = plt.subplots()
            ds_clean.plot()
            fig.text(.7, .2, "Average: $\overline{{d}} = {0:.3g}$\n st. dev: $\sigma_d = {1:.2g}$".format(mean, std))
            plt.xlabel("Time")
            plt.ylabel("Distance [m]")
            plt.title("Raw distance data: Anchor {0} to Tag {1}".format(anchor_num, tag_num))
            plt.grid()
            plt.show()


    print("==============================")
    print("EXPERIMENT " + experiment_idx)
    print("Mean")
    print(df_mean)
    print("")
    print("Standard deviation")
    print(df_std)
    print("")
    print("Relative error (std / mean)")
    print(df_rel_error)
    print("==============================")
    print("")
    print("")

    # df_rel_error.plot(style="o-")
    # plt.xlabel("Tag ID")
    # plt.ylabel("Relative error [dimensionless]")
    # plt.title("Relative error for measurements in experiment " + experiment_idx)
    # plt.grid()

    df_std.plot(style="o-")
    plt.xlabel("Tag ID")
    plt.ylabel("Standard deviation [m]")
    plt.title("Standard deviation for measurements in experiment " + experiment_idx)
    plt.grid()

    # df_mean.plot(style="o-")
    # plt.xlabel("Tag ID")
    # plt.ylabel("Distance [m]")
    # plt.title("Mean distance for measurements in experiment " + experiment_idx)
    # plt.grid()

plt.show()