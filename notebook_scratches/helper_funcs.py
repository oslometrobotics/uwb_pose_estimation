def print_df_size(df, df_name):
    print("{0}  -  Columns: {1}  -  Rows: {2}".format(df_name, len(df.columns), len(df)))
