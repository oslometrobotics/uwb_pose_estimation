# UWB Pose Estimation #

This project contains scripts and tools to estimate the relative pose (position and orientation) between to robots equipped with arrays of UWB beacons. 

Submitted paper to IROS 2017 here:
https://www.overleaf.com/read/rdgsxvbyzsds

### Basic setup instructions on a windows machine ###
The following is a basic setup to run the scripts in a native windows machine using an ubuntu virtual machine. 

* Download ubuntu image (recommended 16.04 LTS) 
* Install virtualbox [https://www.virtualbox.org/](https://www.virtualbox.org/)
* Create ubuntu virtual machine
* Install ROS. Follow instructions in: [http://wiki.ros.org/kinetic/Installation/Ubuntu](http://wiki.ros.org/kinetic/Installation/Ubuntu)
* Install anaconda 2.7. Follow instructions in [https://www.continuum.io/downloads#linux](https://www.continuum.io/downloads#linux)
* Install rosbag_pandas and additional packages: 
~~~~
pip install rosbag_pandas
~~~~
* You may need to additionally install
~~~~
pip install rospkg
pip install catkin_pkg
~~~~
* You can now launch spyder python IDE with
~~~~
spyder
~~~~


### Who do I talk to? ###

alex.alcocer@hioa.no