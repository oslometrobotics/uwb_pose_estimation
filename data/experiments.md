# Experiments - data logging

## Preparation:
- Calibrate system and origo. Long calibration to maximize volume, 10 minutes min.
- Define rigid bodies for 4 Anchors  and 1 tag. 4 markers for each.

- Place 4 anchors in array similar to final configuration. 1m distance between anchors. Centered approx in origo. One of anchors not in same plane.
- Start recording.

## Experiment 1: Calibration of antenna position and local tag/anchors origin

- Place tag horizontal approx 3m from origo and rotate around vertical axis. 3 full rotations. 
- Change position of tag and repeat. Do this for at least 5 different positions of tag.  
- Total duration approx 5 minutes. No need to stop recording while doing all experiments.
- Stop recording and check that all necessary data has been logged successfully.
- Restart data logging.

## Experiment 2: Tag moving inside calibrated volume
- Move tag freely to cover as much as calibrated volume. 
- Include rotations of tag, not always in horizontal plane. 
- Include variation in vertical coordinate of tag. Duration approx 10 minutes.

## Experiment 3: Tag moving in and out of calibrated volume
- Move tag freely around whole bevlab, in and out of calibrated volume. We will loose qualisys tracking when outside volume but data can be useful anyway. This is to test what happens when tag is far from anchors. Include also variation in rotation and vertical coordinate. Duration approx: 10 minutes

## Experiment 4:  
- Change configuration of anchors. Now distance is approx 2m from each other.
- Repeat experiment 2 with new anchor positions. Move tag freely inside calibrated volume. Duration approx 10 minutes

## Experiment 5:
- Repeat experiment 3 with new anchor positions. Move tag freely inside bevlab, try to get as far as possible, explore all corners of bevlab. This would be usefull to see how columns and other obstacles affect signal.

## Experiment 6: Satic measuremnts
- Calibrate qualisys and leave origo in place
- Set 5 anchors around origo and measure manually precisely the position (x,y,z) of each anchor with respect to origo. Use center of antenna as center.Build two antenna arrays and define rigid bodies in qualysis. 
- Write local position of anchors with respecto to origo in a text/csv file
- Measure manually position of 5 tags with respect to local reference frame and save in txt/csv file.
- tags simulate a small land mobile robot with distance approx 0.5 meter between tags.
- Start logging
- Place tag array in a fixed position near anchors (nearest that gives measurements, maybe 1m apart)
- Leave tag array static for 2 minutes (if 100Hz, that will produce approx 1200 data points)
- Move tag array a little farther from anchors in a different orientation (yaw only, roll and pitch not important here)
- Repeat, leave static for 2 minutes.
- Repeat the process for total 6 different positions of tag array each farther away from anchors. The last position is afarther from anchors as possible 
- The first 3 positions inside the calibrated volume. The last 3 outside the calibrated volume. The las

## Experiment 7 and 8:
- Repeat experiments 2 and 3 moving tag array first inside calibrated volume and later in all bevlab. Move slowly, vary yaw. Make small roll pitch variations (max 30 degrees)

## Notes:
Her kommer en kommentar til innsamlet data:
Vi gjorde eksperiment 1 til 5.
Eksperiment 1 ligger i experiment1.bag og experiment1_2.bag.
I experiment1.bag, så slo Qualisys seg av, så kun data fra anker/tag-system ble logget. I experiment1_2.bag, så ser jeg at data fra anker/tag-system kommer med en ganske stor tidsforsinkelse i forhold til Qualisys. Fra experiment2 til experiment5 fikset vi dette, så målingene i disse filene er derfor mest riktige. Hvis vi beveget oss utenfor volumet rundt origo (ca 4m * 2m * 1,5m), så sluttet Qualisys å fungere. Hvis vi skal gjøre noen forsøk på nytt, så tror jeg det er best å gjøre experiement1 med mindre radius enn 3 meter. Du må bruke ROS for å hente ut info fra bag-filene. Gi beskjed hvis det er noe vi skal gjøre med dette.



